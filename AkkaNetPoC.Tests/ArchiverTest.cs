﻿using System;
using System.IO;
using Akka.Actor;
using Akka.TestKit.VsTest;
using AkkaNetPoC.Messages;
using Ionic.Zip;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AkkaNetPoC.Tests
{
    [TestClass]
    public class ArchiverTest : TestKit
    {
        private string m_tempDirectory;

        [TestInitialize]
        public void SetUp()
        {
            m_tempDirectory = Path.GetTempPath();
            m_tempDirectory = Path.Combine(m_tempDirectory, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(m_tempDirectory);
        }

        [TestCleanup]
        public void TearDown()
        {
            if (Directory.Exists(m_tempDirectory))
            {
                Directory.Delete(m_tempDirectory, true);
            }
        }

        [TestMethod]
        public void CreateArchiveWithFewFilesAndSaveIt_NoPrecondition_ShouldCreateEmptyArchive()
        {
            string archivePath = Path.Combine(m_tempDirectory, "package.zip");
            IActorRef archiver = Sys.ActorOf<Archiver>();

            archiver.Tell(new CreateArchiveMessage(archivePath));
            archiver.Tell(new FileReadMessage(@"c:\\a.txt", new byte[] { 1, 6, 88 }));
            archiver.Tell(new FileReadMessage(@"c:\\dir\\a1.dat", new byte[] { 101 }));
            archiver.Tell(SimpleMessages.Done);

            ExpectMsgAllOf(SimpleMessages.Done, SimpleMessages.Done, SimpleMessages.Done);
            Assert.IsTrue(File.Exists(archivePath));
            using (var zip = new ZipFile(archivePath))
            {
                Assert.IsTrue(zip.ContainsEntry("a.txt"));
                Assert.IsTrue(zip.ContainsEntry("a1.dat"));
            }
        }
    }
}