﻿using System;
using System.IO;
using System.Linq;
using Akka.Actor;
using Akka.TestKit;
using Akka.TestKit.VsTest;
using AkkaNetPoC.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AkkaNetPoC.Tests
{
    [TestClass]
    public class PackageCreatorTest : TestKit 
    {
        private string m_tempDirectory;

        [TestInitialize]
        public void SetUp()
        {
            m_tempDirectory = Path.GetTempPath();
            m_tempDirectory = Path.Combine(m_tempDirectory, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(m_tempDirectory);
        }

        [TestCleanup]
        public void TearDown()
        {
            if (Directory.Exists(m_tempDirectory))
            {
                Directory.Delete(m_tempDirectory, true);
            }
        }

        [TestMethod]
        public void ReceivesCreatePackageMessage_NoPreconditions_ShouldCreatePackage()
        {
            DirectoryInfo currentDirectory = new DirectoryInfo(Environment.CurrentDirectory);

            TestProbe reader = CreateTestProbe();
            IActorRef packager = Sys.ActorOf(Props.Create(() => new PackageCreator(reader)));

            packager.Tell(new CreatePackageMessage(currentDirectory.FullName, m_tempDirectory, "t"));

            var files = currentDirectory.GetFiles().Where(x => Path.GetExtension(x.Name) == ".dll" || Path.GetExtension(x.Name) == ".exe");
            foreach (var file in files)
            {
                reader.ExpectMsgFrom<ReadFileMessage>(packager, x => x.FilePath == file.FullName);
                reader.Reply(new FileReadMessage(file.FullName, new byte[] { 100 }));
            }

            ExpectMsg<string>(x => x == SimpleMessages.Done);
            Assert.IsTrue(File.Exists(Path.Combine(m_tempDirectory, "t.zip")));
        }
    }
}