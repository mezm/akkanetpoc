﻿using System;
using System.IO;
using System.Linq;
using Akka.Actor;
using Akka.TestKit.VsTest;
using AkkaNetPoC.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AkkaNetPoC.Tests
{
    [TestClass]
    public class FileReaderTest : TestKit
    {
        [TestMethod]
        public void ReceivesReadFileMessage_FileExists_ShouldSendBackFileContent()
        {
            DirectoryInfo currentDirectory = new DirectoryInfo(Environment.CurrentDirectory);
            FileInfo file = currentDirectory.GetFiles().First();
            byte[] fileContent = File.ReadAllBytes(file.FullName);

            IActorRef reader = Sys.ActorOf<FileReader>();
            reader.Tell(new ReadFileMessage(file.FullName));

            var msg = ExpectMsg<FileReadMessage>();
            CollectionAssert.AreEqual(fileContent, msg.Content);
            Assert.AreEqual(file.FullName, msg.FilePath);
        }

        [TestMethod]
        public void ReceivesReadFileMessage_FileNotExists_ShouldFail()
        {
            IActorRef reader = Sys.ActorOf<FileReader>();
            EventFilter.Exception<IOException>()
                .ExpectOne(() => reader.Tell(new ReadFileMessage("c:\\some-folder\\not-existing-file.txt")));
        }
    }
}