﻿using System.IO;
using Akka.Actor;
using Akka.Event;
using AkkaNetPoC.Messages;
using Ionic.Zip;

namespace AkkaNetPoC
{
    public class Archiver : ReceiveActor
    {
        private static readonly ILoggingAdapter s_logger = Context.GetLogger();

        private ZipFile m_zip;

        public Archiver()
        {
            Receive<CreateArchiveMessage>(x => CreateArchive(x));
        }

        private void CreateArchive(CreateArchiveMessage message)
        {
            s_logger.Info($"Creating archive at {message.ArchivePath}.");

            m_zip = new ZipFile(message.ArchivePath);
            m_zip.Save();

            Become(
                () =>
                {
                    Receive<FileReadMessage>(x => AddFileToArchive(x));
                    Receive<string>(x => SaveArchive(), x => x == SimpleMessages.Done);
                });
        }

        private void AddFileToArchive(FileReadMessage message)
        {
            var filename = Path.GetFileName(message.FilePath);
            s_logger.Info($"Adding file {filename} to archive.");

            m_zip.AddEntry(filename, message.Content);

            m_zip.Save();

            Sender.Tell(SimpleMessages.Done);
        }

        private void SaveArchive()
        {
            s_logger.Info("Saving archive.");

            m_zip.Dispose();

            Sender.Tell(SimpleMessages.Done);
        }
    }
}