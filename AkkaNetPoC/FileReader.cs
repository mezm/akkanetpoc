﻿using System.IO;
using Akka.Actor;
using Akka.Event;
using AkkaNetPoC.Messages;

namespace AkkaNetPoC
{
    public class FileReader : ReceiveActor
    {
        private static readonly ILoggingAdapter s_logger = Context.GetLogger();

        public FileReader()
        {
            Receive<ReadFileMessage>(x => CopyFile(x));
        }

        private void CopyFile(ReadFileMessage message)
        {
            s_logger.Info($"Reading file {message.FilePath}.");

            byte[] content = File.ReadAllBytes(message.FilePath);
            Sender.Tell(new FileReadMessage(message.FilePath, content));

            s_logger.Info($"Read {content.Length} bytes from file {message.FilePath}.");
        }
    }
}