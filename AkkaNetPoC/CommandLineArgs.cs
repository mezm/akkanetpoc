using System;
using System.Collections.Generic;

namespace AkkaNetPoC
{
    public class CommandLineArgs
    {
        public CommandLineArgs(string[] args)
        {
            if (args.Length == 0)
            {
                return;
            }

            if (!ParseInteger(args[0], x => SelfPort = x))
            {
                return;
            }

            if (args.Length == 1)
            {
                ParsedSuccessfully = true;
                return;
            }

            if (args[1] == "run")
            {
                Run = true;
            }
            else
            {
                ParsedSuccessfully = true;
                return;
            }

            var peers = new List<int>();
            for (var i = 2; i < args.Length; i++)
            {
                if (!ParseInteger(args[i], x => peers.Add(x)))
                {
                    return;
                }
            }

            PeerPorts = peers.ToArray();
            ParsedSuccessfully = true;
        }

        public int SelfPort { get; private set; }

        public int[] PeerPorts { get; private set; }

        public bool Run { get; }

        public bool ParsedSuccessfully { get; }

        private static bool ParseInteger(string arg, Action<int> action)
        {
            int result;
            if (int.TryParse(arg, out result))
            {
                action(result);
                return true;
            }

            return false;
        }
    }
}