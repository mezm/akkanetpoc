﻿using System.IO;
using System.Reflection;
using Akka.Actor;
using Akka.Routing;
using AkkaNetPoC.Messages;

namespace AkkaNetPoC
{
    public class LocalCoordinator : ReceiveActor
    {
        private int m_totalStepCount, m_finishedSteps;
        private IActorRef m_sender;

        public LocalCoordinator()
        {
            Receive<string>(x => Collect(), x => x == SimpleMessages.Start);
        }

        private void Collect()
        {
            Sender.Tell(SimpleMessages.Started);

            IActorRef fileReaders = Context.ActorOf(Props.Create<FileReader>().WithRouter(new RandomPool(5)), "file-copier");
            IActorRef selfPackager = Context.ActorOf(Props.Create(() => new PackageCreator(fileReaders)), "self-package-creator");
            IActorRef remotePackager = Context.ActorOf(Props.Create(() => new PackageCreator(fileReaders)), "remote-package-creator");

            string binDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string sambaDirectoryPath = @"\\zfs1\Public\Vitaliy.Ulantikov\LogCollection\Agent\";
            string targetDirectoryPath = @"d:\tmp\akka-test\" + Self.Path.Name;

            ClearTargetFolder(targetDirectoryPath);

            selfPackager.Tell(new CreatePackageMessage(binDirectoryPath, targetDirectoryPath, "self"));
            remotePackager.Tell(new CreatePackageMessage(sambaDirectoryPath, targetDirectoryPath, "remote"));

            m_totalStepCount = 2;
            m_finishedSteps = 0;
            m_sender = Sender;

            Become(() => Receive<string>(x => StepFinished(), x => x == SimpleMessages.Done));
        }

        private static void ClearTargetFolder(string targetDirectoryPath)
        {
            if (Directory.Exists(targetDirectoryPath))
            {
                Directory.Delete(targetDirectoryPath, true);
            }

            Directory.CreateDirectory(targetDirectoryPath);
        }

        private void StepFinished()
        {
            m_finishedSteps++;
            if (m_finishedSteps == m_totalStepCount)
            {
                m_sender.Tell(SimpleMessages.Done);
            }
        }
    }
}