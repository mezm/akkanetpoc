﻿namespace AkkaNetPoC.Messages
{
    public class ReadFileMessage
    {
        public ReadFileMessage(string filePath)
        {
            FilePath = filePath;
        }

        public string FilePath { get; }
    }
}