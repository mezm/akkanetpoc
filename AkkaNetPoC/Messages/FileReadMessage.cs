﻿namespace AkkaNetPoC.Messages
{
    public class FileReadMessage
    {
        public FileReadMessage(string filePath, byte[] content)
        {
            FilePath = filePath;
            Content = content;
        }

        public string FilePath { get; }

        public byte[] Content { get; }
    }
}