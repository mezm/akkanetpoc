﻿namespace AkkaNetPoC.Messages
{
    public static class SimpleMessages
    {
        public const string Start = "start";
        public const string Started = "started";
        public const string Done = "done";

        public const string StartTimeout = "timeout:start";
        public const string FinishTimeout = "timeout:finish";
    }
}