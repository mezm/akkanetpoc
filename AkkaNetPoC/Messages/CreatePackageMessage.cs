﻿namespace AkkaNetPoC.Messages
{
    public class CreatePackageMessage
    {
        public CreatePackageMessage(string sourceDirectoryPath, string destinationDirectoryPath, string packageName)
        {
            SourceDirectoryPath = sourceDirectoryPath;
            DestinationDirectoryPath = destinationDirectoryPath;
            PackageName = packageName;
        }

        public string SourceDirectoryPath { get; }

        public string DestinationDirectoryPath { get; }

        public string PackageName { get; }
    }
}