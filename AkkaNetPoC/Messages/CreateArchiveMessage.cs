﻿namespace AkkaNetPoC.Messages
{
    public class CreateArchiveMessage
    {
        public CreateArchiveMessage(string archivePath)
        {
            ArchivePath = archivePath;
        }

        public string ArchivePath { get; }
    }
}