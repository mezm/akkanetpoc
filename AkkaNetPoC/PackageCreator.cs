﻿using System.IO;
using System.Linq;
using Akka.Actor;
using Akka.Event;
using AkkaNetPoC.Messages;

namespace AkkaNetPoC
{
    public class PackageCreator : ReceiveActor
    {
        private static readonly ILoggingAdapter s_logger = Context.GetLogger();
        private static readonly string[] s_requiredFileExtensions = { ".dll", ".exe" };

        private readonly IActorRef m_fileReader, m_archiver;

        private int m_totalFileCount, m_fileArchived;
        private IActorRef m_sender;

        public PackageCreator(IActorRef fileReader)
        {
            m_fileReader = fileReader;
            m_archiver = Context.ActorOf<Archiver>("archiver");

            Receive<CreatePackageMessage>(x => CollectPackage(x));
        }

        private void CollectPackage(CreatePackageMessage message)
        {
            string packagePath = Path.Combine(message.DestinationDirectoryPath, message.PackageName + ".zip");

            s_logger.Info($"Starting creating package from {message.SourceDirectoryPath} to {packagePath}.");

            DirectoryInfo directory = new DirectoryInfo(message.SourceDirectoryPath);

            FileInfo[] files = directory.GetFiles()
                .Where(x => s_requiredFileExtensions.Contains(Path.GetExtension(x.Name)))
                .ToArray();

            if (!files.Any())
            {
                s_logger.Warning($"No suitable files in directory {message.SourceDirectoryPath}.");

                Sender.Tell(SimpleMessages.Done);
                return;
            }

            m_archiver.Tell(new CreateArchiveMessage(packagePath));

            foreach (var file in files)
            {
                m_fileReader.Tell(new ReadFileMessage(file.FullName));
            }

            m_totalFileCount = files.Length;
            m_fileArchived = 0;
            m_sender = Sender;

            Become(
                () =>
                {
                    Receive<FileReadMessage>(x => AddFileToArchive(x));
                    Receive<string>(x => FileArchived(), x => x == SimpleMessages.Done);
                });
        }

        private void AddFileToArchive(FileReadMessage message)
        {
            m_archiver.Tell(message);
        }

        private void FileArchived()
        {
            m_fileArchived++;
            if (m_fileArchived != m_totalFileCount)
            {
                return;
            }

            m_archiver.Tell(SimpleMessages.Done);

            Become(() => Receive<string>(x => m_sender.Tell(x), x => x == SimpleMessages.Done));
        }
    }
}