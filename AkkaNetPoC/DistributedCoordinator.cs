﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using Akka.Event;
using AkkaNetPoC.Messages;

namespace AkkaNetPoC
{
    public class DistributedCoordinator : ReceiveActor
    {
        private static readonly ILoggingAdapter s_logger = Context.GetLogger();

        private readonly int[] m_peerPorts;
        private IActorRef m_sender;
        private Dictionary<IActorRef, CoordinatorState> m_coordinatorStates;

        public DistributedCoordinator(params int[] peerPorts)
        {
            m_peerPorts = peerPorts;

            Receive<string>(x => Collect(), x => x == SimpleMessages.Start);
        }

        private void Collect()
        {
            var localCoordinator = Context.ActorOf<LocalCoordinator>("coordinator");
            var remoteCoordinators = m_peerPorts.Select(CreateRemoteActor).ToArray();

            localCoordinator.Tell(SimpleMessages.Start);
            m_coordinatorStates = new Dictionary<IActorRef, CoordinatorState>()
            {
                [localCoordinator] = CoordinatorState.NoStarted
            };
            foreach (var coordinator in remoteCoordinators)
            {
                coordinator.Tell(SimpleMessages.Start);
                m_coordinatorStates.Add(coordinator, CoordinatorState.NoStarted);
            }

            m_sender = Sender;

            SetUpStartTimeoutCheck();
            SetUpFinishTimeoutCheck();

            Become(
                () =>
                {
                    Receive<string>(x => CollectionStarted(), x => x == SimpleMessages.Started);
                    Receive<string>(x => CollectionFinished(), x => x == SimpleMessages.Done);
                    Receive<string>(x => CheckAllStarted(), x => x == SimpleMessages.StartTimeout);
                    Receive<string>(x => CheckAllFinished(), x => x == SimpleMessages.FinishTimeout);
                });
        }

        private void CheckAllFinished()
        {
            var notFinished = m_coordinatorStates.Where(x => x.Value != CoordinatorState.Finished).ToArray();
            foreach (var coordinatorState in notFinished)
            {
                s_logger.Warning($"Coordinator {coordinatorState.Key} was unable to finish collection within timeout.");
                Context.Stop(coordinatorState.Key);
            }

            m_sender.Tell(SimpleMessages.Done);
        }

        private void CheckAllStarted()
        {
            var notStarted = m_coordinatorStates.Where(x => x.Value == CoordinatorState.NoStarted).ToArray();
            if (!notStarted.Any())
            {
                // all started
                return;
            }

            foreach (var coordinatorState in notStarted)
            {
                s_logger.Warning($"Unable to get acknowledgment from {coordinatorState.Key}. Resending message.");
                m_coordinatorStates.Remove(coordinatorState.Key);

                var coordinator = CreateRemoteActor(coordinatorState.Key.Path.Address.Port.Value);
                m_coordinatorStates.Add(coordinator, CoordinatorState.NoStarted);

                coordinator.Tell(SimpleMessages.Start);
            }

            SetUpStartTimeoutCheck();
        }

        private void SetUpFinishTimeoutCheck()
        {
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromMinutes(5), Self, SimpleMessages.FinishTimeout, Self);
        }

        private void SetUpStartTimeoutCheck()
        {
            Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(10), Self, SimpleMessages.StartTimeout, Self);
        }

        private void CollectionStarted()
        {
            m_coordinatorStates[Sender] = CoordinatorState.Started;
        }

        private void CollectionFinished()
        {
            m_coordinatorStates[Sender] = CoordinatorState.Finished;

            var finished = m_coordinatorStates.Count(x => x.Value == CoordinatorState.Finished);
            s_logger.Info($"Actor {Sender.Path} finished. [{finished}/{m_coordinatorStates.Count}]");

            Context.Stop(Sender);

            if (finished == m_coordinatorStates.Count)
            {
                s_logger.Info("All actors finished.");
                m_sender.Tell(SimpleMessages.Done);
            }
        }

        private static IActorRef CreateRemoteActor(int port)
        {
            s_logger.Debug($"Creating remote coordinator at port {port}.");

            return Context.ActorOf(
                Props.Create<LocalCoordinator>()
                    .WithDeploy(new Deploy(new RemoteScope(new Address("akka.tcp", "collector", "localhost", port)))), $"coordinator-{port}");
        }

        private enum CoordinatorState
        {
            NoStarted,
            Started,
            Finished
        }
    }
}