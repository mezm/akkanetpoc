﻿using System;
using System.Diagnostics;
using Akka.Actor;
using Akka.Configuration;
using AkkaNetPoC.Messages;

namespace AkkaNetPoC
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var cliArgs = new CommandLineArgs(args);
            if (!cliArgs.ParsedSuccessfully)
            {
                Console.WriteLine($"usage: {Environment.GetCommandLineArgs()[0]} <port> [run] [<peer ports> ...]");
                return;
            }

            using (var system = ActorSystem.Create("collector", GetSystemConfiguration(cliArgs)))
            {
                if (cliArgs.Run)
                {
                    var stopWatch = Stopwatch.StartNew();

                    system.Log.Info("Starting collection");
                    var coordinator = system.ActorOf(Props.Create(() => new DistributedCoordinator(cliArgs.PeerPorts)), "global-coordinator");
                    coordinator.Ask<string>(SimpleMessages.Start).Wait();
                   
                    system.Log.Info($"Collection finished and took {stopWatch.Elapsed}.");
                }

                Console.ReadLine();
            }
        }

        private static Config GetSystemConfiguration(CommandLineArgs args)
        {
            var fallbackConfig = ConfigurationFactory.Load();
            return ConfigurationFactory.ParseString($"akka.remote.helios.tcp.port = {args.SelfPort}").WithFallback(fallbackConfig);
        }   
    }
}
